<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Default controller for homepage and co.
 */
class DefaultController extends AbstractController
{
    #[Route('/', name: 'app_homepage')]
    public function homepage() : Response
    {
        return $this->render('homepage.html.twig', ['phpversion' => phpversion()]);
    }
}
