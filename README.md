# PHP Playground

https://php-playground.duckdns.org/

## Development

Start containers with `make infra-up`  
Enter php container with `make dev`

neovim is available, you need to install the plugins with `:PlugInstall`

## Local URLs

-   http://localhost:8000 the app

## Front

Build with `yarn run build`  
During dev, run encore with: `yarn run encore dev-server --host 0.0.0.0 --disable-host-check`

## PHP language server

If you want intelephense premium features (see https://intelephense.com),
once you have a key, put in the `license.txt` and ignore changes with `git update-index --skip-worktree docker/config/intelephense/license.txt`

## Prod

Test prod container with `docker build -f docker/Dockerfile.prod .` and `docker run --rm -p 8800:80 containerid`

## Resources

https://medium.com/@jayden.chua/automate-deploying-a-php-project-with-docker-gitlab-ci-and-gitlab-repository-97eb7f30b2d6  
http://www.inanzzz.com/index.php/post/isb1/alpine-docker-setup-for-symfony-applications  
https://www.erickpatrick.net/blog/my-neovim-setup-for-php-development  
https://vim-bootstrap.com/
