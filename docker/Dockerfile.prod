FROM php:8.0.0RC4-fpm-alpine3.12

ARG SYMFONY_DECRYPTION_SECRET 
ENV SYMFONY_DECRYPTION_SECRET=$SYMFONY_DECRYPTION_SECRET
ENV APP_ENV=prod

RUN apk add --update --upgrade --no-cache --purge \
    alpine-sdk \
    autoconf \
    bash \
    curl \
    gdb \
    git \
    icu-dev \
    libzip-dev \
    nginx \
    nodejs nodejs-npm \
    procps \
    supervisor \
    unzip \
    zip \
    && rm -rf /var/cache/apk/* /tmp/*

COPY . /var/www/html
COPY docker/config/php/php.ini /usr/local/etc/php/php.ini
COPY docker/config/supervisor/supervisord.conf /etc/supervisor/supervisord.conf
COPY docker/config/nginx/default /etc/nginx/conf.d/default
COPY docker/config/nginx/h5bp/ /etc/nginx/h5bp
COPY docker/config/php/php-fpm.d/docker.conf /usr/local/etc/php-fpm.d/zz-docker.conf
COPY docker/start.sh /usr/local/bin/start

RUN mkdir -p /run/nginx \
    && rm /etc/nginx/conf.d/default.conf \
    && mv /etc/nginx/conf.d/default /etc/nginx/conf.d/default.conf \
    && curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer \
    && chmod +x /usr/local/bin/start \
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log \
    && docker-php-ext-install intl zip \ 
    && docker-php-ext-configure zip \
    && wget https://github.com/FriendsOfPHP/pickle/releases/download/v0.6.0/pickle.phar \
    && mv pickle.phar /usr/local/bin/pickle \
    && chmod +x /usr/local/bin/pickle \
    && pickle install apcu \
    && docker-php-ext-enable apcu \
    && npm install -g neovim yarn \
    && composer install --no-interaction --no-progress --optimize-autoloader --prefer-dist --no-dev \
    && php bin/console secrets:decrypt-to-local --force --env=prod \
    && yarn install \ 
    && yarn run encore production \
    && chown -R www-data:www-data /var/www/html \
    && npm remove yarn \
    && apk del nodejs npm 

WORKDIR /var/www/html

CMD ["/usr/local/bin/start"]
